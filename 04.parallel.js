function looseParallel(tasks, callback) {
  // Tạo ra môt mảng rỗng để chứa các phần tử result 
  const arrayResult = [];
  // Tính kích thước của mảng task
  const taskLength = tasks.length;

  // Tạo một biến chứa index
  let index = 0;

  // Sử dụng vòng lặp for để thực hiện các tasks
  for (let i = 0; i<taskLength; i++) {
    tasks[i](cb);
  }
  
  function cb(err, result) {
    // Sau khi thực hiện xong sẽ push result vào mảng rỗng trên
    arrayResult.push(result);
    // cộng index
    index++;
    // Kiểm tra điều kiện index bằng với kích thước của taskLength
    // Nếu chỉ số index bằng với kích thước taskLength thì sẽ thực hiện gọi hàm callback để show ra kết quả
    if (index == taskLength) {
      callback(null, arrayResult);
    }
  }
}
console.time('looseParallel');
looseParallel([
  (cb) => {
    setTimeout(() => {
      cb(null, 100);
    }, 100);
  }, (cb) => {
    setTimeout(() => {
      cb(null, 50);
    }, 50);
  }, (cb) => {
    setTimeout(() => {
      cb(null, 500);
    }, 500);
  }, (cb) => {
    setTimeout(() => {
      cb(null, 200);
    }, 200);
  }
], (err, result) => {
  console.log(result); // [50, 100, 200, 500] The ordering of the result is based on the consuming time of the task
  console.timeEnd('looseParallel'); // ~500ms
});

// ------------------------------------------------------------- //

function parallel(tasks, callback) {
  // Tạo một mảng để chứa các result
  const arrayResult = [];
  // Tính kích thước của tasks
  const taskLength = tasks.length;

  // Tạo một biến chứa index
  let index = 0;
  
  // Dùng vòng lặp for để thực hiện các tasks
  for (let i = 0; i<taskLength; i++) {
    // task thực hiện truyền vào một function cb
    tasks[i](cb);
    function cb(a, timer) {
      // Khi cb hiện thực xong có kết quả sẽ đưa vào mảng arrayResult
      arrayResult[i] = timer;
      // cộng index
      index++;
      // Kiểm tra điều kiện index và kích thước của mảng taskLength
      // Nếu chỉ số index bằng với kích thước mảng taskLength thì sẽ thực hiện gọi hàm callback để show ra kết quả
      if (index == taskLength) {
        callback(a,arrayResult);
      }
    }
  }
}
console.time('Parallel');
parallel([
  (cb) => {
    setTimeout(() => {
      cb(null, 100);
    }, 100);
  }, (cb) => {
    setTimeout(() => {
      cb(null, 50);
    }, 50);
  }, (cb) => {
    setTimeout(() => {
      cb(null, 500);
    }, 500);
  }, (cb) => {
    setTimeout(() => {
      cb(null, 200);
    }, 200);
  }
], (err, result,index) => {
  console.log(result); // [100, 50, 500, 200] the ordering of the result is as same as the tasks input
  console.timeEnd('Parallel'); // ~500ms
});