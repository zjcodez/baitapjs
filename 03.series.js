function series(tasks, callback) {
    // Tạo ra môt mảng rỗng để chứa các phần tử result
    const arrayResult = [];

    // Tạo một biến i có giá trị = 0
    let i = 0;

    // Tính kích thước của mảng task
    const taskLength = tasks.length - 1;
    
    // Thực thiện task đầu tiên trong mảng
    tasks[i](cb);

    function cb(err, result){
        // Sau khi thực hiện xong sẽ push result vào mảng rỗng trên
        arrayResult.push(result);

        // Kiểm tra điều kiện nếu i hiện tại nhỏ hơn kích thuóc của tasks thì thực hiện task tiếp theo
        // Bằng cách cộng i
        if(i < taskLength){ 
            i++;
            tasks[i](cb);
        } else {
            // Ngược lại i lớn hơn hoặc bằng với kích thước của tasks thì thực hiện gọi callback
            return callback(null, arrayResult);
        }
    }
}

console.time('Series');
series([
    (cb) => {
        setTimeout(() => {
            cb(null, 100);
        }, 100);
    }, (cb) => {
        setTimeout(() => {
            cb(null, 50);
        }, 50);
    }, (cb) => {
        setTimeout(() => {
            cb(null, 200);
        }, 200);
    }, (cb) => {
        setTimeout(() => {
            cb(null, 500);
        }, 500);
    }
], (err, result) => {
    console.log(result); // [100, 50, 200, 500]
    console.timeEnd('Series'); // ~ 100 + 50 + 200 + 500 = 850ms
});